$("#login-form").on("submit", function (e) {
    e.preventDefault();
    executeLogin($('#input-login').val(), $('#input-password').val());
    return false;
});

function executeLogin(login, password) {
    const base64Credentials = btoa(login + ":" + password);
    $.ajax({
        type: 'GET',
        headers: {
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': true,
            'Authorization': 'Basic ' + base64Credentials,
            'Set-Cookie':true
        },
        url: sarverUrl + 'dashboard/',
        success: function (data, textStatus, xhr) {
            console.log(xhr.status);
            localStorage.setItem("auth", base64Credentials);
            localStorage.setItem("user-type", login);
            loadDashboard();
        },
        crossDomain: true,
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status);
            if(jqXHR.status == 401){
                $('#input-login').addClass("is-invalid");
                $('#input-password').addClass("is-invalid")
            } else if(jqXHR.status != 200) {
                window.location.href = "errorPage.html";
            }
        }
    });

    return false;
}