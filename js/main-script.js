let mainContainer;
const doc = document;
const sarverUrl = "http://localhost:8080/";

main();

function main(){
    initialize();

    loadDashboard();
}

function loadDashboard(){
    if(localStorage.getItem("user-type") === "admin"){
        loadDashboardAdmin()
    } else {
        loadDashboardUser()
    }
}

function initialize() {
    mainContainer = $('#main-container');
}

function loadLoginContainer(){
    mainContainer.innerHTML = "";
    $(mainContainer).load("containers/login.html");
}
function checkLogin(){
    if(localStorage.getItem("auth") != undefined || localStorage.getItem("auth") == "") {
        return true;
    } else {
        loadLoginContainer();
        return false;
    }
}

function loadDashboardUser(){
    if(checkLogin()){
        mainContainer.innerHTML = "";
        $(mainContainer).load("containers/userDashboard.html");
    }
}

function loadDashboardAdmin(){
    if(checkLogin()){
        mainContainer.innerHTML = "";
        $(mainContainer).load("containers/adminDashboard.html");
    }
}

function logout(){
    localStorage.clear();
    loadLoginContainer();
}