loadBooks();

function loadBooks(){
    $.ajax({
        type: 'GET',
        headers: {
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': true,
            'Authorization': 'Basic ' + localStorage.getItem("auth"),
            'Set-Cookie':true
        },
        url: sarverUrl + 'dashboard/',
        success: function (data, textStatus, xhr) {
            console.log(xhr.status);
            console.log(data);
            drawBookList(data)
            dat = data;

        },
        crossDomain: true,
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status);
            if(jqXHR.status == 401){
                loadLoginContainer();
            } else if(jqXHR.status != 200) {
                //window.location.href = "errorPage.html";
            }
        }
    });
}

function drawBookList(list){
    for(let i = 0, max = list.length; i < max; i++){
        $('#booksList').append('<li class="list-group-item">' + list[i]['title'] +
        ' - ' + list[i]['title'] + ' (' + list[i]['year'] +') </li>' )
    }
}