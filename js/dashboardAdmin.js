loadBooks();

function loadBooks(){
    $.ajax({
        type: 'GET',
        headers: {
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': true,
            'Authorization': 'Basic ' + localStorage.getItem("auth"),
            'Set-Cookie':true
        },
        url: sarverUrl + 'dashboard/',
        success: function (data, textStatus, xhr) {
            console.log(xhr.status);
            console.log(data);
            drawBookList(data)
            dat = data;

        },
        crossDomain: true,
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status);
            if(jqXHR.status == 401){
                loadLoginContainer();
            } else if(jqXHR.status != 200) {
                window.location.href = "errorPage.html";
            }
        }
    });
}

function drawBookList(list){
    $('#booksList').html("");
    for(let i = 0, max = list.length; i < max; i++){
        $('#booksList').append('<li class="list-group-item">' + list[i]['title'] +
        ' - ' + list[i]['title'] + ' (' + list[i]['year'] +')' +
            '<button class="btn btn-danger float-right" onclick="deleteBook(' +
            list[i]['id'] + ')">Usuń</button> </li>' )
    }
}

function deleteBook(id){
    $.ajax({
        type: 'DELETE',
        headers: {
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': true,
            'Authorization': 'Basic ' + localStorage.getItem("auth"),
            'Set-Cookie':true
        },
        url: sarverUrl + 'dashboard/' + id,
        success: function (data, textStatus, xhr) {
            console.log(xhr.status);
            console.log(data);
            loadBooks();
        },
        crossDomain: true,
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status);
            if(jqXHR.status == 401){
                loadLoginContainer();
            } else if(jqXHR.status != 200) {
                window.location.href = "errorPage.html";
            }
        }
    });
}

function addBook(){
    console.log('title' + $('#input-title').val());
    if ($('#input-title').val() == "" ||
    $('#input-author').val() == "" ||
    $('#input-year').val() == "" ) {
        alert("Nie wypełniłeś wszystkich pól!!");
        return false;
    }

    $.ajax({
        type: 'POST',
        processData: false,
        contentType: 'application/json',
        data:JSON.stringify({
            'title':$('#input-title').val(),
            'author':$('#input-author').val(),
            'year':parseInt($('#input-year').val())
        }),
        headers: {
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': true,
            'Authorization': 'Basic ' + localStorage.getItem("auth"),
            'Set-Cookie':true
        },
        url: sarverUrl + 'dashboard/',
        success: function (data, textStatus, xhr) {
            console.log(xhr.status);
            console.log(data);
            $('#input-title').val("");
            $('#input-author').val("");
            $('#input-year').val("");
            loadBooks();
        },
        crossDomain: true,
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status);
            if(jqXHR.status == 401){
                loadLoginContainer();
            } else if(jqXHR.status != 200) {
                //window.location.href = "errorPage.html";
            }
        }
    });
}